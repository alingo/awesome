-- Load requisite libraries
dofile("/home/alingo/.config/awesome/requires.lua")

-- Load helper functions
dofile("/home/alingo/.config/awesome/functions.lua")


hostname = os.capture("hostname")

-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/themes/" .. hostname .. "/theme.lua")

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, false)
    end
end
-- }}}