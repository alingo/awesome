-- Load requisite libraries
dofile("/home/alingo/.config/awesome/requires.lua")

-- Load helper functions
dofile("/home/alingo/.config/awesome/functions.lua")

-- Handle runtime errors
dofile("/home/alingo/.config/awesome/error-handling.lua")

-- Run startup programs
dofile("/home/alingo/.config/awesome/startup.lua")

-- Load the theme
dofile("/home/alingo/.config/awesome/theme-loader.lua")

-- {{{ Variable definitions
dofile("/home/alingo/.config/awesome/variables.lua")


-- Table of layouts to cover with awful.layout.inc, order matters.
layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    --awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen
    --awful.layout.suit.magnifier
}
-- }}}



-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    --tags[s] = awful.tag({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }, s, layouts[1])
    tags[s] = awful.tag({ 1, 2, 3, 4, 5 }, s, layouts[1])
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
--
menu_divider = {"-----------------", nil}

myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

function start_term_with_command(command_str)
  full_str = terminal .. ' -e ' .. '\'' .. shell .. ' -c \"' .. command_str .. '\";' .. shell .. '-i\''
  return full_str
end


mygomenu = {
   { "&lumi", start_term_with_command('ssh alingo@lumi')},
   { "&thelingo", start_term_with_command('ssh alingo@thelingo.org')}
}

mygomenumenu = awful.menu({ items = mygomenu })

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "&go...", mygomenu },
                                    menu_divider,
                                    { "&terminal", terminal },
                                    { "firefox", 'firefox'},
                                    menu_divider,
                                    { "lock screen", lock_cmd}

                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox
-- Create a textclock widget
--

function updatebattbox(box)
  os = require('os')
  local batt_status = os.capture("cat /sys/class/power_supply/BAT0/status")
  local batt_percentage = ""
  local batt_value = tonumber(os.capture("cat /sys/class/power_supply/BAT0/capacity"))
  if batt_status == "Discharging" then
      batt_prefix = beautiful.battery_discharging_prefix
      batt_suffix = beautiful.battery_discharging_suffix
      batt_percentage = "<span color='" .. beautiful.battery_discharging_color .. "'>"
  else
      batt_prefix = beautiful.battery_charging_prefix
      batt_suffix = beautiful.battery_charging_suffix
      batt_percentage = "<span color='" .. beautiful.battery_charging_color .. "'>"
  end
  if batt_value <= 15  and batt_status == "Discharging" then
    batt_percentage = batt_percentage .. "DANGER "
  end

  batt_percentage = batt_prefix .. batt_percentage .. batt_value .. "%" .. batt_suffix .. "</span>"
  box:set_markup(batt_percentage)
end

if hostname == "monumental" then
  mybattbox = wibox.widget.textbox("")
  updatebattbox(mybattbox)
end

mytextclock = awful.widget.textclock("%A %B %d, %I:%M %p")
mypaddingbox = wibox.widget.textbox(" ")
mydivider = wibox.widget.textbox("<span size='9000' rise='1250'  color='" .. beautiful.borderbox_color .. "'>|</span>")
--mydivider = wibox.widget.textbox("<span size='12000' rise='2250'  color='" .. beautiful.borderbox_color .. "'>|</span>")


-- Create a wibox for each screen and add it
myborderbox = {}
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({
                                                      theme = { width = 250 }
                                                  })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    mylayoutbox[s] = awful.widget.layoutbox(s)
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))

    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s, width = "100%", align = "center" })
    myborderbox[s] = lain.widgets.borderbox(mywibox[s], s, {position = "bottom", color = beautiful.borderbox_color, size = beautiful.borderbox_width})

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(mypaddingbox)
    left_layout:add(mytaglist[s])
    left_layout:add(mypaddingbox)
    left_layout:add(mydivider)
    left_layout:add(mypaddingbox)
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then right_layout:add(wibox.widget.systray()) end
    right_layout:add(mydivider)
    right_layout:add(mypaddingbox)
    if hostname == 'monumental' then
      right_layout:add(mybattbox)
      right_layout:add(mypaddingbox)
      right_layout:add(mydivider)
      right_layout:add(mypaddingbox)
    end
    right_layout:add(mytextclock)
    right_layout:add(mypaddingbox)
    right_layout:add(mydivider)
    right_layout:add(mypaddingbox)
    right_layout:add(mylayoutbox[s])
    right_layout:add(mypaddingbox)

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- Start Timers for updating widgets
function set_widget_timers()
  mybatttimer = timer({ timeout = 5 })
  mybatttimer:connect_signal("timeout", function() updatebattbox(mybattbox) end)
  mybatttimer:start()
end

set_widget_timers()

dofile(awful.util.getdir("config") .. "/" .. "keybindings.lua")

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     size_hints_honor = false,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "xfce4-notifyd" }, callback = function(c) awful.titlebar.hide(c) end },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    -- Set Firefox to always map on tags number 2 of screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { tag = tags[1][2] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    elseif not c.size_hints.user_position and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count change
        awful.placement.no_offscreen(c)
    end

    titlebars_enabled = true
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        -- left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:add(awful.titlebar.widget.floatingbutton(c))
        left_layout:add(awful.titlebar.widget.stickybutton(c))
        left_layout:add(awful.titlebar.widget.ontopbutton(c))

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.minimizebutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
	close_btn = awful.titlebar.widget.closebutton(c)
	tt_close = awful.tooltip({ objects = {close_btn },})
	tt_close:set_text('Close window')
	tt_close:add_to_object(close_btn)
        right_layout:add(close_btn)

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        -- local title = awful.titlebar.widget.titlewidget(c)
        local title = awful.titlebar.widget.titlewidget(c)
	title.name = "FIN"
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)
        awful.titlebar(c, {
            bg_normal = beautiful.titlebar_bgcolor_normal,
            fg_normal = beautiful.titlebar_fgcolor_normal,
            bg_focus = beautiful.titlebar_bgcolor_focus,
            fg_focus = beautiful.titlebar_fgcolor_focus,
            }):set_widget(layout)
        --awful.titlebar(c):set_widget(layout)

	--Add tooltips
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
