---------------------------
-- Default awesome theme --
---------------------------

theme = {}

theme.tasklist_disable_icon = true

theme.battery_charging_color = "#008800"
theme.battery_discharging_color = "#880000"

theme.font          = "sans 8"

theme.bg_normal     = "#000000"
theme.bg_focus      = "#000000"
theme.bg_urgent     = "#9D2D3C"
theme.bg_minimize   = "#000000"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#333333"
theme.borderbox_width = 1
theme.borderbox_color =  '#860000'

theme.titlebar_bgcolor_normal = theme.bg_normal
theme.titlebar_fgcolor_normal = theme.fg_normal
theme.titlebar_bgcolor_focus = "#390000"
theme.titlebar_fgcolor_focus = theme.fg_normal


theme.border_width  = 0
theme.border_normal = theme.titlebar_bgcolor_normal
theme.border_focus  = theme.titlebar_bgcolor_focus
theme.border_marked = "#91231c"

--theme.borderbox_color = "#86BBD8"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Display the taglist squares
theme.taglist_squares_sel   = "/home/alingo/.config/awesome/themes/ashford/taglist/squarefw.png"
theme.taglist_squares_unsel = "/home/alingo/.config/awesome/themes/ashford/taglist/squarew.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = "/home/alingo/.config/awesome/themes/ashford/submenu.png"
theme.menu_height = 15
theme.menu_width  = 100

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = "/home/alingo/.config/awesome/themes/ashford/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = "/home/alingo/.config/awesome/themes/ashford/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = "/home/alingo/.config/awesome/themes/ashford/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = "/home/alingo/.config/awesome/themes/ashford/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "/home/alingo/.config/awesome/themes/ashford/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = "/home/alingo/.config/awesome/themes/ashford/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "/home/alingo/.config/awesome/themes/ashford/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = "/home/alingo/.config/awesome/themes/ashford/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "/home/alingo/.config/awesome/themes/ashford/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = "/home/alingo/.config/awesome/themes/ashford/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = "/home/alingo/.config/awesome/themes/ashford/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = "/home/alingo/.config/awesome/themes/ashford/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "/home/alingo/.config/awesome/themes/ashford/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = "/home/alingo/.config/awesome/themes/ashford/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = "/home/alingo/.config/awesome/themes/ashford/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = "/home/alingo/.config/awesome/themes/ashford/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = "/home/alingo/.config/awesome/themes/ashford/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = "/home/alingo/.config/awesome/themes/ashford/titlebar/maximized_focus_active.png"

theme.wallpaper = true

theme.wallpaper =  "/home/alingo/.config/awesome/themes/ashford/ashford_background.png"

-- You can use your own layout icons like this:
theme.layout_fairh = "/home/alingo/.config/awesome/themes/ashford/layouts/fairh.png"
theme.layout_fairv = "/home/alingo/.config/awesome/themes/ashford/layouts/fairv.png"
theme.layout_floating  = "/home/alingo/.config/awesome/themes/ashford/layouts/floating.png"
theme.layout_magnifier = "/home/alingo/.config/awesome/themes/ashford/layouts/magnifier.png"
theme.layout_max = "/home/alingo/.config/awesome/themes/ashford/layouts/max.png"
theme.layout_fullscreen = "/home/alingo/.config/awesome/themes/ashford/layouts/fullscreen.png"
theme.layout_tilebottom = "/home/alingo/.config/awesome/themes/ashford/layouts/tilebottom.png"
theme.layout_tileleft   = "/home/alingo/.config/awesome/themes/ashford/layouts/tileleft.png"
theme.layout_tile = "/home/alingo/.config/awesome/themes/ashford/layouts/tile.png"
theme.layout_tiletop = "/home/alingo/.config/awesome/themes/ashford/layouts/tiletop.png"
theme.layout_spiral  = "/home/alingo/.config/awesome/themes/ashford/layouts/spiral.png"
theme.layout_dwindle = "/home/alingo/.config/awesome/themes/ashford/layouts/dwindle.png"

theme.awesome_icon = "/home/alingo/.config/awesome/themes/ashford/mainmenu16.png"

-- Define the icon theme for application icons. If not set then the icons 
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
