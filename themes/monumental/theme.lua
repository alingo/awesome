---------------------------
-- Default awesome theme --
---------------------------

-- Base colors to draw from
-- http://paletton.com/#uid=5530u0k8eoT2dE24+tzbGkTe-g2
shadeA0 = "#946E8A"
shadeA1 = "#D1C2CD"
shadeA2 = "#B094A8"
shadeA3 = "#7C4F70"
shadeA4 = "#5F3453"

shadeB0 = "#BB8B93"
shadeB1 = "#F5E4E7"
shadeB2 = "#DEBCC1"
shadeB3 = "#9D646D"
shadeB4 = "#79424A"

shadeC0 = "#776685"
shadeC1 = "#BDB7C3"
shadeC2 = "#94889F"
shadeC3 = "#5E4A70"
shadeC4 = "#453256"

theme = {}

theme.tasklist_disable_icon = true

theme.battery_charging_color = "#ffffff"
theme.battery_discharging_color = "#ffffff"
theme.battery_charging_prefix = "⌁"
theme.battery_charging_suffix = ""
theme.battery_discharging_suffix = "🔋"
theme.battery_discharging_prefix = ""

theme.font          = "sans 8"

theme.bg_normal_static     = shadeA4
theme.bg_normal_linear =    {  type = "linear", from = { 0, 0 }, to = { 0, 20 },
                      stops = { { 0, shadeA4 .. "ff" },{ 0.95, shadeA2 .. "cc" }, { 1, "#00000000" } } }
theme.bg_normal = theme.bg_normal_linear
theme.bg_focus      = shadeA3 .. 'cc'
theme.bg_urgent     = "#9D2D3Cdd"
theme.bg_minimize   = shadeA3 .. 'ee'
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#333333"

theme.titlebar_fgcolor_normal = theme.gf_normal
theme.titlebar_bgcolor_normal = theme.bg_normal
theme.titlebar_bgcolor_focus = '#946e8acc'
theme.titlebar_fgcolor_focus = '#ffffff'

theme.borderbox_width = 1
theme.borderbox_color = "#000000cc"

theme.border_width  = 1
theme.border_normal = "#00000080"
theme.border_focus  = theme.titlebar_bgcolor_focus
theme.border_marked = "#000000dd"

theme.tasklist_bg_focus = theme.titlebar_bgcolor_focus
theme.tasklist_fg_focus = theme.titlebar_fgcolor_focus
theme.tasklist_bg_minimize = theme.bg_minimize

--theme.borderbox_color = "#86BBD8"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Display the taglist squares
-- theme.taglist_squares_sel   = "/home/alingo/.config/awesome/themes/monumental/taglist/squarefw.png"
-- theme.taglist_squares_unsel = "/home/alingo/.config/awesome/themes/monumental/taglist/squarew.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_bg_normal = theme.bg_normal_static
theme.menu_bg_focus = theme.bg_focus
theme.menu_submenu_icon = "/home/alingo/.config/awesome/themes/monumental/submenu.png"
theme.menu_height = 15
theme.menu_width  = 100

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = "/home/alingo/.config/awesome/themes/monumental/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = "/home/alingo/.config/awesome/themes/monumental/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = "/home/alingo/.config/awesome/themes/monumental/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = "/home/alingo/.config/awesome/themes/monumental/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "/home/alingo/.config/awesome/themes/monumental/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = "/home/alingo/.config/awesome/themes/monumental/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "/home/alingo/.config/awesome/themes/monumental/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = "/home/alingo/.config/awesome/themes/monumental/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "/home/alingo/.config/awesome/themes/monumental/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = "/home/alingo/.config/awesome/themes/monumental/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = "/home/alingo/.config/awesome/themes/monumental/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = "/home/alingo/.config/awesome/themes/monumental/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "/home/alingo/.config/awesome/themes/monumental/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = "/home/alingo/.config/awesome/themes/monumental/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = "/home/alingo/.config/awesome/themes/monumental/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = "/home/alingo/.config/awesome/themes/monumental/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = "/home/alingo/.config/awesome/themes/monumental/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = "/home/alingo/.config/awesome/themes/monumental/titlebar/maximized_focus_active.png"


theme.wallpaper = "/home/alingo/.config/awesome/themes/monumental/monumental_background.png"

-- You can use your own layout icons like this:
theme.layout_fairh = "/home/alingo/.config/awesome/themes/monumental/layouts/fairh.png"
theme.layout_fairv = "/home/alingo/.config/awesome/themes/monumental/layouts/fairv.png"
theme.layout_floating  = "/home/alingo/.config/awesome/themes/monumental/layouts/floating.png"
theme.layout_magnifier = "/home/alingo/.config/awesome/themes/monumental/layouts/magnifier.png"
theme.layout_max = "/home/alingo/.config/awesome/themes/monumental/layouts/max.png"
theme.layout_fullscreen = "/home/alingo/.config/awesome/themes/monumental/layouts/fullscreen.png"
theme.layout_tilebottom = "/home/alingo/.config/awesome/themes/monumental/layouts/tilebottom.png"
theme.layout_tileleft   = "/home/alingo/.config/awesome/themes/monumental/layouts/tileleft.png"
theme.layout_tile = "/home/alingo/.config/awesome/themes/monumental/layouts/tile.png"
theme.layout_tiletop = "/home/alingo/.config/awesome/themes/monumental/layouts/tiletop.png"
theme.layout_spiral  = "/home/alingo/.config/awesome/themes/monumental/layouts/spiral.png"
theme.layout_dwindle = "/home/alingo/.config/awesome/themes/monumental/layouts/dwindle.png"

theme.awesome_icon = "/home/alingo/.config/awesome/themes/monumental/mainmenu16.png"

-- Define the icon theme for application icons. If not set then the icons 
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
